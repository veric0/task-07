package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;

public class DBManager {
	private static final String CONNECTION_URL = "jdbc:derby:memory:testdb;create=true";

	private static DBManager instance;
	private static Connection connection;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			if (connection == null) {
				try {
				connection = DriverManager.getConnection(CONNECTION_URL);
				} catch (SQLException throwables) {
					throwables.printStackTrace();
				}
			}
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		try (Statement statement = connection.createStatement()) {
			String query = "select * from users";
			ResultSet resultSet = statement.executeQuery(query);
			List<User> users = new LinkedList<>();
			int id;
			String login;
			User user;
			while (resultSet.next()) {
				id = resultSet.getInt("id");
				login = resultSet.getString("login");
				user = User.createUser(login);
				user.setId(id);
				users.add(user);
			}
			return users;
		} catch (SQLException throwables) {
			throw new DBException("sql exception in DBManager#findAllUsers", throwables);
		}
	}

	public boolean insertUser(User user) throws DBException {
		if (user == null) {
			throw new IllegalArgumentException("bad input in DBManager#insertUser");
		}
		String query = "insert into users values (DEFAULT, '" + user.getLogin() + "')";
		try (PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
			int rowcount = statement.executeUpdate();
			if (rowcount == 1) {
				try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
					if (generatedKeys.next()) {
						user.setId((int)generatedKeys.getLong(1));
					}
					else {
						throw new SQLException("Creating user failed, no ID obtained.");
					}
				}
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException("sql exception in DBManager#insertUser", throwables);
		}
		return false;
	}

	public boolean deleteUsers(User... users) throws DBException {
		if (users == null || users.length == 0) {
			throw new IllegalArgumentException("bad input in DBManager#deleteTeam");
		}
		String query = "delete from users where id = ?";
		try (PreparedStatement statement = connection.prepareStatement(query)) {
			for (User user :users) {
				statement.setInt(1, user.getId());
				int rowcount = statement.executeUpdate();
				if (rowcount != 1) {
					return false;
				}
			}
			return true;
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException("sql exception in DBManager#insertTeam", throwables);
		}
	}

	public User getUser(String login) throws DBException {
		if (login == null) {
			throw new IllegalArgumentException("bad input in DBManager#getUser");
		}
		try (Statement statement = connection.createStatement()) {
			String query = "select id from users where login = '" + login + "'";
			ResultSet resultSet = statement.executeQuery(query);
			if (resultSet.next()) {
				User user = User.createUser(login);
				user.setId(resultSet.getInt("id"));
				return user;
			}
		} catch (SQLException throwables) {
			throw new DBException("sql exception in DBManager#getUser", throwables);
		}
		return null;
	}

	public Team getTeam(String name) throws DBException {
		if (name == null) {
			throw new IllegalArgumentException("bad input in DBManager#getTeam");
		}
		try (Statement statement = connection.createStatement()) {
			String query = "select id from teams where name = '" + name + "'";
			ResultSet resultSet = statement.executeQuery(query);
			if (resultSet.next()) {
				Team team = Team.createTeam(name);
				team.setId(resultSet.getInt("id"));
				return team;
			}
		} catch (SQLException throwables) {
			throw new DBException("sql exception in DBManager#getUser", throwables);
		}
		return null;
	}

	public List<Team> findAllTeams() throws DBException {
		try (Statement statement = connection.createStatement()) {
			String query = "select * from teams";
			ResultSet resultSet = statement.executeQuery(query);
			return listOfTeams(resultSet);
		} catch (SQLException throwables) {
			throw new DBException("sql exception in DBManager#findAllTeams", throwables);
		}
	}

	public boolean insertTeam(Team team) throws DBException {
		if (team == null) {
			throw new IllegalArgumentException("bad input in DBManager#insertTeam");
		}
		String query = "insert into teams values (DEFAULT, '" + team.getName() + "')";
		try (PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
			int rowcount = statement.executeUpdate();
			if (rowcount == 1) {
				try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
					if (generatedKeys.next()) {
						team.setId((int)generatedKeys.getLong(1));
					}
					else {
						throw new SQLException("Creating team failed, no ID obtained.");
					}
				}
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException("sql exception in DBManager#insertTeam", throwables);
		}
		return false;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		if (user == null || teams == null || teams.length == 0) {
			throw new IllegalArgumentException("bad input in DBManager#setTeamsForUser");
		}
		String query = "insert into users_teams values (?,?)";
		List<Team> newTeamsForThisUser = new ArrayList<>(List.of(teams));
		newTeamsForThisUser.removeAll(getUserTeams(user));
		if (newTeamsForThisUser.size() < 1 || newTeamsForThisUser.size() < teams.length) throw new DBException("nothing to commit in DBManager#setTeamsForUser");
		try (PreparedStatement statement = connection.prepareStatement(query)) {
			connection.setAutoCommit(false);
			int rowsAffected;
			for (Team team : newTeamsForThisUser) {
				statement.setInt(1, user.getId());
				statement.setInt(2, team.getId());
				rowsAffected = statement.executeUpdate();
				if (rowsAffected != 1) {
					connection.rollback();
					connection.setAutoCommit(true);
					return false;
				}
			}
			connection.commit();
			connection.setAutoCommit(true);
			return true;
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException("sql exception in DBManager#setTeamsForUser: ", throwables);
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		if (user == null) {
			throw new IllegalArgumentException("bad input (user is null) in DBManager#getUserTeams");
		}
		user = getUser(user.getLogin());
		if (user == null) {
			throw new IllegalArgumentException("bad input (there is no user with this login in the table) in DBManager#getUserTeams");
		}
		try (Statement statement = connection.createStatement()) {
			String query = "select id, name from users_teams inner join teams on teams.id = users_teams.team_id where user_id = " + user.getId();
			ResultSet resultSet = statement.executeQuery(query);
			return listOfTeams(resultSet);
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException("sql exception in DBManager#getUserTeams", throwables);
		}
	}

	public boolean deleteTeam(Team team) throws DBException {
		if (team == null) {
			throw new IllegalArgumentException("bad input in DBManager#deleteTeam");
		}
		team = getTeam(team.getName());
		if (team == null) {
			throw new IllegalArgumentException("bad input (there is no team with this name in the table) in DBManager#deleteTeam");
		}
		try (Statement statement = connection.createStatement()) {
			String query = "delete from teams where id = " + team.getId();
			int rowcount = statement.executeUpdate(query);
			if (rowcount == 1) {
				return true;
			}
		} catch (SQLException throwables) {
			throw new DBException("sql exception in DBManager#insertTeam", throwables);
		}
		return false;
	}

	public boolean updateTeam(Team team) throws DBException {
		if (team == null) {
			throw new IllegalArgumentException("bad input in DBManager#deleteTeam");
		}
		try (Statement statement = connection.createStatement()) {
			String query = "update teams set name = '" + team.getName() + "' where id = " + team.getId();
			int rowcount = statement.executeUpdate(query);
			if (rowcount == 1) {
				return true;
			}
		} catch (SQLException throwables) {
			throw new DBException("sql exception in DBManager#insertTeam", throwables);
		}
		return false;
	}

	private List<Team> listOfTeams(ResultSet resultSet) throws SQLException {
		List<Team> teams = new LinkedList<>();
		int id;
		String name;
		Team team;
		while (resultSet.next()) {
			id = resultSet.getInt("id");
			name = resultSet.getString("name");
			team = Team.createTeam(name);
			team.setId(id);
			teams.add(team);
		}
		return teams;
	}

}
